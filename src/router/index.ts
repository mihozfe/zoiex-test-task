// Composables
import { createRouter, createWebHistory } from 'vue-router'
import {Home} from './home'
import {Details} from './details'

const routes = [
  Home,
  Details,
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
