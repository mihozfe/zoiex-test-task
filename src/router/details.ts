import type {RouteRecordRaw} from 'vue-router'
import DetailsTemplate from '@/layouts/default/DetailsTemplate.vue'

export const Details: RouteRecordRaw = {
  path: '/',
  component: DetailsTemplate,
  children: [
    {
      path: 'gifs/:id',
      name: 'gif-details',
      component: ()=>import('@/views/GifDetails.vue')
    },
    {
      path: 'author/:name',
      name: 'author',
      component: ()=>import('@/views/AuthorDetails.vue')
    }
  ]
}