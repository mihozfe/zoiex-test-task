import type {RouteRecordRaw} from "vue-router"
import DefaultTemplate from '@/layouts/default/Default.vue'

export const Home: RouteRecordRaw = 
{
  path: '/',
  component: DefaultTemplate,
  children: [
    {
      name: 'home',
      path: '/',
      component:() => import('@/views/Home.vue'),
    }
  ]
}