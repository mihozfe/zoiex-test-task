import { ref, watchEffect, toValue } from 'vue'

export const getData = (url: () => string) => {
  const data = ref<any>(null)
  const error = ref<any>(null)
  const loading = ref<boolean>(false)


  const fetchData = () => {
    fetch(`${import.meta.env.VITE_API_URL}${toValue(url)}&${new URLSearchParams({api_key: import.meta.env.VITE_GIPHY_KEY})}`)
      .then(response => response.json())
      .then(json => data.value = json)
      .catch((err) => {
        error.value = err
      })
      .finally(() => {
        loading.value = false
      })
  }

  watchEffect(() => {
    data.value = null
    fetchData()
  })
  return { error, data, loading }
}
