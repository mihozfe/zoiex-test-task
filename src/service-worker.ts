class ServiceWorkerOne {

  public static run(): void {
      addEventListener('install', ServiceWorkerOne.onInstalled);
      addEventListener('fetch', ServiceWorkerOne.onFetched);
  }

  public static onInstalled = (event: any): void => {
      event.waitUntil(
          caches.open('v0.1').then((cache) => {
              return cache.addAll([
                  '/assets/logo.png',
                  '/assets/index.css',
                  '/assets/index.js',
                  '/index.html'
              ]);
          })
      );
  }

  public static onFetched = (event: any): void => {
      event.respondWith(
          caches.match(event.request).then((matchResponse) => {
              return matchResponse || fetch(event.request).then((fetchResponse) => {
                  return caches.open('v0.1').then((cache) => {
                      cache.put(event.request, fetchResponse.clone());
                      return fetchResponse;
                  });
              });
          })
      );
  }
}

ServiceWorkerOne.run();