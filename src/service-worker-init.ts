function registerServiceWorker(): void {
  if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('/src/service-worker.js')
          .then((registration) =>
              console.log(`Service Worker registration complete, scope: '${registration.scope}'`))
          .catch((error) =>
              console.log(`Service Worker registration failed with error: '${error}'`));
  }
}

registerServiceWorker();